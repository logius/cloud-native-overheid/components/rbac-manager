# Overall  functionality

## Install RBAC Manager

The rbac-manager role installs the rbac-manager Operator in the cluster.

See https://github.com/FairwindsOps/rbac-manager